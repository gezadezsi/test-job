# Version: 0.1
## Each instruction commit a layer to the image

FROM debian:jessie

MAINTAINER EchoMany Ltd "geza.dezsi@gmail.com"

SHELL ["/bin/bash", "-c"]

# IMPORTANT !!
# In order to have a shared external_sources across docker building
# the context of the building the the root of the repository
# not the directory where Dockerfile resides

COPY build_scripts/install_*.sh /root/
COPY build_scripts/setup_apache.sh /root/

RUN /root/install_dependencies.sh

COPY code/project /var/www

RUN /root/install_and_run_composer.sh

ENV DOMAIN_NAME echomany.dev

RUN /root/setup_apache.sh

COPY build_scripts/php.ini /etc/php5/fpm/php.ini

COPY build_scripts/Procfile /root/Procfile

COPY build_scripts/start_command.sh /usr/bin/

EXPOSE 80 443

ENTRYPOINT ["/usr/bin/start_command.sh"]

## HEALTHCHECK command allow to define how to check everything is working as expected
