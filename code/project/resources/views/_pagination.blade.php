@if($collection->hasPages())

    <?php

    $currentPage = $collection->currentPage();
    $maxPages = ceil($collection->total() / $collection->perPage());

    ?>



    <div class="ui pagination menu">
        {{$collection->appends(Input::except('page'))->links()}}
    </div>

    <div class="pagination-container">
        <div class="float-left">Show me</div>
        <div class="float-left">
            <div class="ui pagination menu">
                @php $local_url = route('users.index', [
                                        'page' => Input::has('page')?Input::get('page'):1,
                                        'sort_by' => $sort_by,
                                        'sort_order'=> $sort_order
                                    ]);
                @endphp
                <select name="display"
                        onchange="
                                this.options[this.selectedIndex].value && (window.location =  '{{$local_url}}&display='+this.options[this.selectedIndex].value);">
                    <option value="10" {{ ($display == 10 ? "selected":"") }}>10</option>
                    <option value="20" {{ ($display == 20 ? "selected":"") }}>20</option>
                    <option value="50" {{ ($display == 50 ? "selected":"") }}>50</option>
                    <option value="100" {{ ($display == 100 ? "selected":"") }}>100</option>
                </select>
            </div>
        </div>
        <div class="float-left">users per page</div>
    </div>

    <style>
        .pagination-container {
            height: 2em;
        }
        .pagination-container div{
            padding: 5px;
        }

        .ui.menu.pagination{
            border: 0px;
            box-shadow: none;
        }
        .ui.pagination.menu.pagination li{
            float: left;
            list-style-type: none;
            margin: 0 5px;
            padding: 5px 10px;
            border: 1px solid rgba(34, 36, 38, 0.15);
            box-shadow: 0px 1px 2px 0 rgba(34, 36, 38, 0.15);
            border-radius: 0.28571429rem;
        }
    </style>


@endif()