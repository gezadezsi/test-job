@extends('layout')

@section('content')
    @if(Session::has('message'))
        <div class="alert alert-{{Session::get('status')}}">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="row form-group">
        <div class="col-md-12">
            {{ Form::open(array('url' => route('users.index'),'method'=>'get','class'=>'form-inline')) }}
                <div class="form-group">
                    {{Form::label('search', 'Search for user')}}
                </div>
                <div class="form-group mx-sm-3">
                    {{Form::text('search', old('search'),['class'=>'form-control','placeholder'=>'Name, email... '])}}
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>

                <div class="form-group">
                    &nbsp;OR&nbsp; <span class="btn btn-info" data-toggle="collapse" data-target="#advanced_search_form">Advanced search</span>
                </div>
                <div class="form-group">
                    &nbsp;OR&nbsp; <a href="{{route('users.index')}}" class="btn btn-warning">Reset</a>
                </div>
            {{ Form::close() }}
        </div>

    </div>
    <div id="advanced_search_form" class="collapse @if(Input::has('advanced_search')) show @endif">
        <div class="row form-group">
            <div class="col-md-12">
                <hr />
                    {{ Form::open(array('url' => route('users.index'),'method'=>'get')) }}
                        <input type="hidden" name="advanced_search" value="1" />
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                {{Form::label('address_1', 'Address 1')}}
                                {{Form::text('address_1', old('address_1'),['class'=>'form-control','placeholder'=>'Address 1'])}}
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('address_2', 'Address 2')}}
                                {{Form::text('address_2', old('address_2'),['class'=>'form-control','placeholder'=>'Address 2'])}}
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                {{Form::label('town', 'Town')}}
                                {{Form::text('town', old('town'),['class'=>'form-control','placeholder'=>'Town'])}}
                            </div>
                            <div class="form-group col-md-4">
                                {{Form::label('county', 'County')}}
                                {{Form::text('county', old('county'),['class'=>'form-control','placeholder'=>'County'])}}
                            </div>
                            <div class="form-group col-md-2">
                                {{Form::label('age_from', 'Age from')}}
                                {{Form::text('age_from', old('age_from'),['class'=>'form-control','placeholder'=>'Age from'])}}
                            </div>
                            <div class="form-group col-md-2">
                                {{Form::label('age_to', 'Age to')}}
                                {{Form::text('age_to', old('age_to'),['class'=>'form-control','placeholder'=>'Age to'])}}
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                    {{ Form::close() }}
                <hr />
            </div>

        </div>
    </div>
    @include('_pagination', ['collection' => $users])

    <hr />
        <div class="form-group">
            &nbsp;<a href="{{route('users.create')}}" class="btn btn-primary">Add new user</a>
        </div>
    <hr />
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">
                {{ link_to_route('users.index', '#', [
                        'page' => Input::get('page'),
                        'sort_by' => 'id',
                        'sort_order'=> $sort_order,
                        'display'=>$display
                    ])
                 }}
            </th>
            <th scope="col">
                {{ link_to_route('users.index', 'First name', [
                        'page' => Input::get('page'),
                        'sort_by' => 'first_name',
                        'sort_order'=> $sort_order,
                        'display'=>$display
                    ])
                 }}
            </th>
            <th scope="col">
                {{ link_to_route('users.index', 'Last name', [
                        'page' => Input::get('page'),
                        'sort_by' => 'last_name',
                        'sort_order'=> $sort_order,
                        'display'=>$display
                    ])
                 }}
            </th>
            <th scope="col">
                {{ link_to_route('users.index', 'Age', [
                        'page' => Input::get('page'),
                        'sort_by' => 'age',
                        'sort_order'=> $sort_order,
                        'display'=>$display
                    ])
                 }}
            </th>
            <th scope="col">
                {{ link_to_route('users.index', 'Email', [
                        'page' => Input::get('page'),
                        'sort_by' => 'email',
                        'sort_order'=> $sort_order,
                        'display'=>$display
                    ])
                 }}
            </th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->first_name}}</td>
                <td>{{$user->last_name}}</td>
                <td>{{$user->age}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <a href="{{ route('users.view',['user_id'=>$user->id]) }}" class="btn btn-primary">Edit</a>
                </td>
                <td>
                    {{ Form::open(array('url' => route('users.delete',['user_id'=>$user->id]),'method'=>'post','class'=>'form-inline')) }}
                        <input type="hidden" name="return_url" value="{{Request::getQueryString()}}">
                        <button type="submit" class="btn btn-danger js-delete-button">Delete</button>
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <hr />

    @include('_pagination', ['collection' => $users])
@endsection

@section('footer_js')
    <script>
        jQuery('.js-delete-button').on('click',function(e){
            if(confirm("Are you sure?"))
                return true;
            else
                return false;
        });
    </script>
@endsection