@extends('layout')

@section('content')
    <a href="{{route('users.index')}}"> <<< Back</a>

        <h1>
            @if ( isset($user) )
                Editing '{{ $user->first_name }} {{$user->last_name}}'
            @else
                Add a new user
            @endif
        </h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{ Form::open(array('url' => $url,'method'=>'post')) }}
        <div class="form-row">
            <div class="form-group col-md-6">
                {{Form::label('first_name', 'First name')}}
                {{Form::text('first_name',isset($user) ? old('first_name', $user->first_name) : old('first_name'),['class'=>'form-control'])}}
            </div>
            <div class="form-group col-md-6">
                {{Form::label('last_name', 'Last name')}}
                {{Form::text('last_name',isset($user) ? old('last_name', $user->last_name) : old('last_name'),['class'=>'form-control'])}}

            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                {{Form::label('email', 'Email')}}
                {{Form::text('email',isset($user) ? old('email', $user->email) : old('email'),['class'=>'form-control'])}}

            </div>
            <div class="form-group col-md-6">
                {{Form::label('age', 'Age')}}
                {{Form::text('age',isset($user) ? old('age', $user->age) : old('age'),['class'=>'form-control'])}}

            </div>
        </div>
        <div class="form-group">
            {{Form::label('address_1', 'Address line 1')}}
            {{Form::text('address_1',isset($user) ? old('address_1', $user->address_1) : old('address_1'),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('address_2', 'Address line 2')}}
            {{Form::text('address_2',isset($user) ? old('address_2', $user->address_2) : old('address_2'),['class'=>'form-control'])}}
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                {{Form::label('town', 'Town')}}
                {{Form::text('town',isset($user) ? old('town', $user->town) : old('town'),['class'=>'form-control'])}}

            </div>
            <div class="form-group col-md-4">
                {{Form::label('county', 'County')}}
                {{Form::text('county',isset($user) ? old('county', $user->county) : old('county'),['class'=>'form-control'])}}

            </div>
            <div class="form-group col-md-2">
                {{Form::label('post_code', 'Postcode')}}
                {{Form::text('post_code',isset($user) ? old('post_code', $user->post_code) : old('post_code'),['class'=>'form-control'])}}

            </div>
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
    {{ Form::close() }}

@endsection