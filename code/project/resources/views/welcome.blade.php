@extends('layout')

@section('content')
    <h1>Welcome visitor!</h1>

    <p>This is a little Laravel demonstration site with a User database and a CRUD interface. <br />
        It has Bootstrap CSS framework integrated for a better user experience.
    </p>
    <p>
        <a href="{{route('users.index')}}">Click here</a> to see the User list interface.
    </p>
@endsection