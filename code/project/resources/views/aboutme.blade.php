@extends('layout')

@section('content')
    <h2>Welcome visitor!</h2>

    <p>
        My name is Geza Dezsi, PHP backend developer located in Reading. I'm mainly focused on Laravel development on LAMP environment,
        Docker virtualisation and cloud architecture management.
    </p>
    <p>
        You can contact me in <a href="mailto:geza.dezsi@gmail.com">email</a>.
    </p>

@endsection