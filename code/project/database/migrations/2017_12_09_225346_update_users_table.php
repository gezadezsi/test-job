<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name',255)->nullable(true)->change();
            $table->string('email',255)->nullable(true)->change();
            $table->string('address_2',255)->nullable(true)->change();
            $table->string('county',255)->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name',255)->nullable(false)->change();
            $table->string('email',255)->nullable(false)->change();
            $table->string('address_2',255)->nullable(false)->change();
            $table->string('county',255)->nullable(false)->change();
        });
    }
}
