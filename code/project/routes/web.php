<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 *
 * STATIC PAGES
 *
 */

Route::get('/', function() {
    return view('welcome');
});

Route::get('/about-me', function() {
    return view('aboutme');
});


/*
 *
 * USER MANAGEMENT ROUTES
 *
 */

// LIST USERS
Route::get('/users', ['as' => 'users.index', 'uses' => 'UserController@listUsers']);

// CREATE NEW USER
Route::post('/users', ['as' => 'users.view', 'uses' => 'UserController@updateUser']);

// DISPLAY FORM TO CREATE NEW USER
Route::get('/users/create', ['as' => 'users.create', 'uses' => function() {
    return view('users.view', ['url' => '/users']);
}]);

//DISPLAY USER
Route::get('/users/{user_id}', ['as' => 'users.view', 'uses' => 'UserController@viewUser']);

// UPDATE USER
Route::post('/users/{user_id}', ['as' => 'users.view', 'uses' => 'UserController@updateUser']);

// DELETE USER
Route::post('/users/{user_id}/delete', ['as' => 'users.delete', 'uses' => 'UserController@deleteUser']);
