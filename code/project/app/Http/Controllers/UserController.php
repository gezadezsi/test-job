<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;


use App\Models\User;

class UserController extends BaseController
{
    use  ValidatesRequests;


    /**
     * List users and manage pagination, search and display
     *
     * @param Request $request The request object to get the search and display informations
     * @return User list view
     */
    public function listUsers(Request $request)
    {
        $query = new User();

        $filters = [];

        if ($request->has('search')) {
            $filters['search'] = $request->get('search');
            $query = $query->where('first_name', 'LIKE', "%{$filters['search']}%");
            $query = $query->orWhere('last_name', 'LIKE', "%{$filters['search']}%");
            $query = $query->orWhere('email', 'LIKE', "%{$filters['search']}%");
        }

        if ($request->has('advanced_search')) {
            foreach($request->all() as $key => $value) {
                if($key != 'advanced_search' && $value != '') {
                    if($key == 'age_from')
                        $query = $query->Where('age','>=',"{$value}");
                    elseif($key == 'age_to')
                        $query = $query->Where('age','<=',"{$value}");
                    else
                        $query = $query->orWhere($key,'LIKE',"%{$value}%");
                }

            }
        }


        $paginate = 10;
        if ($request->has('display')) {
            $paginate = $request->get('display');
        }

        $sort_by = 'id';
        $sort_order = 'ASC';

        if ($request->has('sort_by')) {
            $sort_by = $request->get('sort_by');
        }

        if ($request->has('sort_order')) {
            $sort_order = $request->get('sort_order');
        }

        $users = $query->orderBy($sort_by, $sort_order)->paginate($paginate);

        if ($sort_order == 'ASC') {
            $sort_order = 'DESC';
        } else {
            $sort_order = 'ASC';
        }

        return view('users.list', [
            'users' => $users->appends($filters),
            'filters' => $filters,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'display' => $paginate
        ]);
    }

    /**
     *  Display a single user, used on GET request
     *
     * @param $user_id The ID of the user
     * @return User view with the update url pregenerated
     */
    public function viewUser($user_id)
    {

        $user = User::find($user_id);

        return view('users.view', ['user' => $user, 'url' => '/users/' . $user->id]);
    }


    /**
     *  Update a single user, used on POST request
     *
     * @param Request $request The request object
     * @param $user_id = '' The ID of the user, default empty string, if empty then create a new user
     * @return redirect to user.list action on success or user.view action on error
     */
    public function updateUser(Request $request, $user_id = '')
    {
        $this->validate($request, [
            'last_name' => 'required',
            'age' => 'numeric|min:18|max:65',
            'address_1' => 'required',
            'town' => 'required',
            'post_code' => 'required',
        ]);

        if ($user_id != '') {
            $user = User::find($user_id);
        } else {
            $user = new User();
        }

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->age = $request->input('age');
        $user->address_1 = $request->input('address_1');
        $user->address_2 = $request->input('address_2');
        $user->town = $request->input('town');
        $user->county = $request->input('county');
        $user->post_code = $request->input('post_code');


        if ($user->save()) {
            $message = 'User created.';
            if ($user_id != '') {
                $message = 'User updated.';
            }


            return redirect()->route('users.index')->with([
                'status' => 'success',
                'message' => $message
            ]);
        } else {
            if ($user_id != '') {
                return redirect()->route('users.view', ['user_id' => $user_id])->with([
                    'status' => 'warning',
                    'message' => 'Something went wrong.'
                ]);
            } else {
                return redirect()->route('users.view', ['user_id' => $user_id])->with([
                    'status' => 'warning',
                    'message' => 'Something went wrong.'
                ]);
            }
        }
    }

    /**
     *  Delete a single user, used on POST request
     *
     * @param Request $request The request object
     * @param $user_id The ID of the user to delete
     * @return redirect to user.list action on success or user.view action on error
     */
    public function deleteUser(Request $request, $user_id)
    {
        $return_array = [];
        $return_query_string = $request->input('return_url');
        $store = explode('&', $return_query_string);

        if ($store) {
            foreach ($store as $item) {
                $internal_store = explode('=', $item);
                if (isset($internal_store[0]) && isset($internal_store[1])) {
                    $return_array[$internal_store[0]] = $internal_store[1];
                }
            }
        }

        $user = User::find($user_id);

        if ($user->delete()) {
            return redirect()->route('users.index', $return_array)->with([
                'status' => 'success',
                'message' => 'User deleted.'
            ]);
        } else {
            return redirect()->route('users.view', ['user_id' => $user_id])->with([
                'status' => 'warning',
                'message' => 'Something went wrong.'
            ]);
        }
    }

}
